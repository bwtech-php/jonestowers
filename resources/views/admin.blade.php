@extends('layouts.admin')

@section('content')
<router-view :auth='@json(Auth::user())'></router-view>
@endsection