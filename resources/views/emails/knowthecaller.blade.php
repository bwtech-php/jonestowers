{{-- @component('mail::message')
# Introduction

The body of your message.

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent --}}


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Document</title>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:separate">
	<tbody>
		<tr>
			<td align="center" bgcolor="#fff" style="padding:0px 20px">
				<table bgcolor="#f3f3f3" border="0" cellpadding="0" cellspacing="0" style="max-width:600px;border-collapse:separate;" width="100%">
					<tbody>
						<tr>
							<td>
								<table style="background-image:url(https://i.imgur.com/Va2xtWA.png); background-repeat:no-repeat; background-position:left bottom;" width="100%">
									<tbody style="padding: 20px 18px;display:inline-block;">
										<tr>
											<td>
												<img src="https://i.imgur.com/Rxihrx8.png">
											</td>
										</tr>
										<tr>
											<td style="padding-top: 20px; padding-bottom:207px;">
												<img src="https://i.imgur.com/1UxFpde.png">
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="100%">
									<tbody>
										<tr>
											<td align="center" style="padding: 17px 18px 0;font-family: 'Poppins', sans-serif !important;font-size:20px;font-weight:500;color:#4B5CA9;line-height:8px;">KNOW WHO OWNS</td>
										</tr>
										<tr>
											<td align="center" style="padding: 0px 18px 15px;font-family: 'Poppins', sans-serif !important;font-size:26px;font-weight:500;color:#4B5CA9">ANY PHONE NUMBER!</td>
										</tr>
										<tr>
											<td align="center" style="padding: 0px 18px 15px;font-family: 'Poppins', sans-serif !important;font-size:14px;font-weight:400;color:#4B5CA9">Need to KNOW Who a Phone Number belongs to? Need to know Who Keeps Calling you? Need to protect your Children, Teenagers, Spouses, Friends, Relatives, and Neighbors from prank phone calls and harassing texts? Want to be the one that -Knows- the <br>information? Then Let Knowelle help you root out all the information on any phone number with <span style="color:#F05771; text-decoration: none;">KnowTheCaller</span></td>
										</tr>
										<tr>
											<td align="center" style="padding: 30px 18px 0;font-family: 'Poppins', sans-serif !important;font-size:18px;font-weight:500;color:#4B5CA9">Find the information You are looking for - 99.9% Accurate results Direct from the Carriers!</td>
										</tr>
										<tr>
											<td align="center" style="padding: 10px 18px 10px;font-family: 'Poppins', sans-serif !important;font-size:14px;font-weight:400;color:#4B5CA9">Need to KNOW Who a Phone Number belongs to? Need to know Who Keeps Calling you? Need to protect your Children, Teenagers, Spouses, Friends, Relatives, and Neighbors from prank phone calls and harassing texts? Want to be the one that -Knows- the information? Then you need <span style="color:#F05771; text-decoration: none;">KnowTheCaller</span> to root out all the information on any phone number.</td>
										</tr>
										<tr>
											<td align="center" style="padding:0 18px;">
												<a href="https://www.google.com/" target="_blank" style="background-color:#FCB415; font-size:14px; font-family: 'Poppins', sans-serif !important; font-weight:400;line-height:30px; color:#fff;text-decoration: none;    display: inline-block;width: 210px;border-radius: 100px;text-align: center;margin: 0 10px;">SUBSCRIBE FOR FREE</a>
												<a href="https://www.google.com/" target="_blank" style="background-color:#F05771; font-size:14px; font-family: 'Poppins', sans-serif !important; font-weight:400;line-height:30px; color:#fff;text-decoration: none;    display: inline-block;width: 210px;border-radius: 100px;text-align: center;margin: 0 10px;">UNSUBSCRIBE</a>
											</td>
										</tr>
                                        <td align="center">
                                            <br><small><a href="https://www.google.com/" target="_blank">Can't view this Email? View it in the browser</a></small>
                                        </td>
										<tr align="right" style="padding:10px 18px;display:block;">
											<td><img src="https://i.imgur.com/1UxFpde.png"></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
				
			</td>
		</tbody>
	</table>
</body>
</html>