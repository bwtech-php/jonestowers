<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetails extends Model
{
    public function products()
    {
        return $this->belongsTo('App\Product');
    }
}
