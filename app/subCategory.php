<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subCategory extends Model
{
    public function products()
    {
        return $this->hasMany('App\Product', 'sub_category_id');
    }
}
