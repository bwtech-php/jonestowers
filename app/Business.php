<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $guarded = [];
    
    public function reviews()
    {
        // return $this->hasMany('App\Review')->where('id', 2);
        return $this->hasMany('App\Review')->join('users', 'reviews.user_id', '=', 'users.id')->select('reviews.*', 'users.user_name as user_name', 'users.image as user_image');
    }

    public function businessImages()
    {
        return $this->hasMany('App\BusinessImage');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }
}
