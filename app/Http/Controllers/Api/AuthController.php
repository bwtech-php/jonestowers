<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Payment;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Cashier\Cashier;

//Mail
use Illuminate\Support\Facades\Mail;
use App\Mail\PinVerification;

class AuthController extends Controller
{
    
    // public function registerUser(Request $request) {
        
    //     try {
            
    //         $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET', false));
            
    //        $validatedData = $request->validate([
    //         'user_name' => 'required|max:55',
    //         'email' => 'email|required',
    //         'password' => 'required'
    //         ]);

    //         if(User::where('email', $request->email)->exists()) {
    //             return response()->json(['status' => false, 'message' => 'User already exists! Please log in.']);
    //         }
    //         else {
    //             $user = User::create([
    //             'user_name' => $request->user_name,
    //             'email' => $request->email,
    //             'password' => Hash::make($request->password),
    //             'verification_pin' => rand(1000, 9999),
    //             'phone' => $request->phone,
    //             'address' => $request->address,
    //             'city' => $request->city,
    //             'state' => $request->state,
    //             'zip' => $request->zip,
    //             'role' => 'user',
    //             'approved' => 1,
    //         ]);
    //         }

    //         // Mail::to($user)->send(new PinVerification($user));
            
    //         $accessToken =  $user->createToken('registration token');
            
    //         $stripeCustomer = $stripe->customers->create([
    //           'name' => $user->user_name,
    //           'email' => $user->email,
    //         ]);
            
    //         $user->stripe_id = $stripeCustomer->id;
    //         $user->save();
            
    //         $user = $this->setData('user_data', $user->toArray());

    //         $data = ['user' => $user['user_data']];
    //         return response()->json(['status' => true, 'message' => 'Registration successful! Please log in.', 'data' => $data]); 
    //     } catch (\Exception $e) {
    //             return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
    //     }
        
        
    // }

    public function registerUser(Request $request) {
        
        try {
            
            // $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET', false));
            
           $validatedData = $request->validate([
            'user_name' => 'required|max:55',
            'email' => 'email|required',
            'password' => 'required'
            ]);

            if(User::where('email', $request->email)->exists()) {
                return response()->json(['status' => false, 'message' => 'User already exists! Please log in.']);
            }
            
            // $stripeCustomer = $stripe->customers->create([
            //   'name' => $request->user_name,
            //   'email' => $request->email,
            // ]);

            //Get Customer
            // $customer =  $stripe->customers->retrieve(
            //     $stripeCustomer->id,
            //     []
            // );
            // return $customer;

            //Create Payment Method
            // $paymentMethod =  $stripe->paymentMethods->create([
            //     'type' => 'card',
            //     'card' => [
            //         'number' => $request->card_number,
            //         'exp_month' => $request->exp_month,
            //         'exp_year' => $request->exp_year,
            //         'cvc' => $request->cvc,
            //     ],
            // ]);
            // return $paymentMethod;

            //Attach Payment Method to Customer
            // $customerPaymentMethod =  $stripe->paymentMethods->attach(
            //     $paymentMethod->id,
            //     ['customer' => $customer->id]
            // );
            // return $customerPaymentMethod;

            //Create Payment Intent
            // $paymentIntent = $stripe->paymentIntents->create([
            //     'amount' => 1000,
            //     'currency' => 'usd',
            //     'payment_method_types' => ['card'],
            //     'confirm' => true,
            //     'customer' => $customer->id,
            //     'payment_method' => $paymentMethod->id,
            //     'description' => '$1 subscription fee',
            // ]);

            // if($paymentIntent->charges->data[0]->paid == true) {
                $user = User::create([
                    'user_name' => $request->user_name,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'verification_pin' => rand(1000, 9999),
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'city' => $request->city,
                    'state' => $request->state,
                    'zip' => $request->zip,
                    'role' => 'user',
                    'fee_paid' => 1,
                ]);
                
                // $user->stripe_id = $stripeCustomer->id;
                $user->save();

                //Saving card details
                // $cardDetails = new Payment;
                // $cardDetails->user_id = $user->id;
                // $cardDetails->transaction_id = $paymentIntent->id;
                // // $cardDetails->order_id = $order->id;
                // $cardDetails->amount = $paymentIntent->amount_received;
                // $cardDetails->currency = $paymentIntent->charges->data[0]->currency;
                // $cardDetails->card_number = $request->card_number;
                // $cardDetails->exp_month = $request->exp_month;
                // $cardDetails->exp_year = $request->exp_year;
                // $cardDetails->cvc = Hash::make($request->cvc);
                // $cardDetails->payment_status = 1;
                // $cardDetails->save();
            // }
            
            // Mail::to($user)->send(new PinVerification($user));
            
            $accessToken =  $user->createToken('registration token');
            
            $user = $this->setData('user_data', $user->toArray());

            $data = ['user' => $user['user_data']];
            // return response()->json(['status' => true, 'message' => 'Payment and Registration successful! Please log in.', 'data' => $data]);
            return response()->json(['status' => true, 'message' => 'Registration successful! Please log in.', 'data' => $data]);
        } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
        
        
    }

    public function registerBusiness(Request $request) {
        
        try {
            
            // $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET', false));
            
           $validatedData = $request->validate([
            'user_name' => 'required|max:55',
            'email' => 'email|required',
            'password' => 'required'
            ]);

            if(User::where('email', $request->email)->exists()) {
                return response()->json(['status' => false, 'message' => 'User already exists! Please log in.']);
            }
            
            // $stripeCustomer = $stripe->customers->create([
            //     'name' => $request->user_name,
            //     'email' => $request->email,
            // ]);

            //Get Customer
            // $customer =  $stripe->customers->retrieve(
            //     $stripeCustomer->id,
            //     []
            // );

            //Create Payment Method
            // $paymentMethod =  $stripe->paymentMethods->create([
            //     'type' => 'card',
            //     'card' => [
            //         'number' => $request->card_number,
            //         'exp_month' => $request->exp_month,
            //         'exp_year' => $request->exp_year,
            //         'cvc' => $request->cvc,
            //     ],
            // ]);
            
            //Attach Payment Method to Customer
            // $customerPaymentMethod =  $stripe->paymentMethods->attach(
            //     $paymentMethod->id,
            //     ['customer' => $customer->id]
            // );

            //Create Payment Intent
            // $paymentIntent = $stripe->paymentIntents->create([
            //     'amount' => $request->amount * 100,
            //     'currency' => 'usd',
            //     'payment_method_types' => ['card'],
            //     'confirm' => true,
            //     'customer' => $customer->id,
            //     'payment_method' => $paymentMethod->id,
            //     'description' => 'registration fee',
            // ]);

            // if($paymentIntent->charges->data[0]->paid == true) {
                $user = User::create([
                    'user_name' => $request->user_name,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'verification_pin' => rand(1000, 9999),
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'city' => $request->city,
                    'state' => $request->state,
                    'zip' => $request->zip,
                    'url' => $request->url,
                    'lat' => $request->lat,
                    'lng' => $request->lng,
                    'category_id' => $request->category,
                    'african_american_owned' => $request->african_american_owned,
                    'role' => 'business',
                ]);

                // $user->stripe_id = $stripeCustomer->id;
                $user->save();

                //Saving card details
                // $cardDetails = new Payment;
                // $cardDetails->user_id = $user->id;
                // $cardDetails->transaction_id = $paymentIntent->id;
                // // $cardDetails->order_id = $order->id;
                // $cardDetails->amount = $paymentIntent->amount_received;
                // $cardDetails->currency = $paymentIntent->charges->data[0]->currency;
                // $cardDetails->card_number = $request->card_number;
                // $cardDetails->exp_month = $request->exp_month;
                // $cardDetails->exp_year = $request->exp_year;
                // $cardDetails->cvc = Hash::make($request->cvc);
                // $cardDetails->payment_status = 1;
                // $cardDetails->save();
            // }

            // Mail::to($user)->send(new PinVerification($user));
            
            $accessToken =  $user->createToken('registration token');
            
            $user = $this->setData('user_data', $user->toArray());

            $data = ['user' => $user['user_data']];
            return response()->json(['status' => true, 'message' => 'Request received successfully! We will get back to you shortly', 'data' => $data]); 
        } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
        
        
    }

    public function login(Request $request) {
        
        try {
            $request->validate([
                'email' => 'required|email',
                'password' => 'required',
                // 'device_name' => 'required',
            ]);

        $user = User::where('email', $request->email)->first();

         if (! $user || ! Hash::check($request->password, $user->password)) {
            // throw ValidationException::withMessages([
            // 'email' => ['The provided credentials are incorrect.'],
            //  ]);
             return response()->json(['status' => false, 'message' => 'The provided credentials are incorrect.']);
        }

        $token = $user->createToken('login token');
        $user = $this->setData('user_data', $user->toArray());
        $data = ['user' => $user['user_data'], 'token' => $token->plainTextToken];
        return response()->json(['status' => true, 'message' => 'You have logged in successfully!', 'data' => $data]);
        } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        } 
        
    }

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }
}
