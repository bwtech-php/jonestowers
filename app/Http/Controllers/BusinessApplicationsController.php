<?php

namespace App\Http\Controllers;

use App\BusinessApplications;
use Illuminate\Http\Request;

class BusinessApplicationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BusinessApplications  $businessApplications
     * @return \Illuminate\Http\Response
     */
    public function show(BusinessApplications $businessApplications)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BusinessApplications  $businessApplications
     * @return \Illuminate\Http\Response
     */
    public function edit(BusinessApplications $businessApplications)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BusinessApplications  $businessApplications
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BusinessApplications $businessApplications)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BusinessApplications  $businessApplications
     * @return \Illuminate\Http\Response
     */
    public function destroy(BusinessApplications $businessApplications)
    {
        //
    }
}
