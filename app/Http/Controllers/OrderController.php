<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use App\OrderDetails;
use App\Payment;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {              
        try {
            $stripe = new \Stripe\StripeClient("sk_test_51HzrpsDUoUTP6Bs1k4X8aOLDZZEwboEbDzsjTR6n4SoARaM7BFeiuZzubp7Rx9jsYdhVcYRRuGsxmpaFIzvCBHe000hScStPVr");
            
            //Get Customer
            $customer =  $stripe->customers->retrieve(
                Auth::user()->stripe_id,
                []
            );
            // return $customer;

            //Create Payment Method
            $paymentMethod =  $stripe->paymentMethods->create([
                'type' => 'card',
                'card' => [
                    'number' => $request->card_number,
                    'exp_month' => $request->exp_month,
                    'exp_year' => $request->exp_year,
                    'cvc' => $request->cvc,
                ],
            ]);
            // return $paymentMethod;

            //Attach Payment Method to Customer
            $customerPaymentMethod =  $stripe->paymentMethods->attach(
                $paymentMethod->id,
                ['customer' => $customer->id]
            );
            // return $customerPaymentMethod;

            //Create Payment Intent
            $paymentIntent = $stripe->paymentIntents->create([
                'amount' => $request->grandTotal,
                'currency' => 'usd',
                'payment_method_types' => ['card'],
                'confirm' => true,
                'customer' => $customer->id,
                'payment_method' => $paymentMethod->id,
                'description' => 'New Order',
            ]);

            if($paymentIntent->charges->data[0]->paid == true) {
                //Creating new order
                $order = new Order;
                $order->user_id = Auth::id();
                $order->subTotal = $request->subTotal;
                $order->grandTotal = $request->grandTotal;
                $order->address = $request->address;
                $order->payment_status = 1;
                $order->order_status = 1;
                $order->save();

                //Saving card details
                $cardDetails = new Payment;
                $cardDetails->user_id = Auth::id();
                $cardDetails->transaction_id = $paymentIntent->id;
                $cardDetails->order_id = $order->id;
                $cardDetails->amount = $paymentIntent->amount_received;
                $cardDetails->currency = $paymentIntent->charges->data[0]->currency;
                $cardDetails->card_number = $request->card_number;
                $cardDetails->exp_month = $request->exp_month;
                $cardDetails->exp_year = $request->exp_year;
                $cardDetails->cvc = Hash::make($request->cvc);
                $cardDetails->payment_status = 1;
                $cardDetails->save();

                // $productArray=json_decode($request->productArray);
                // $quantityArray=json_decode($request->quantityArray);
                // $variationValueArray=json_decode($request->variationValueArray);

                $productArray=$request->productArray;
                $quantityArray=$request->quantityArray;
                $variationValueArray=$request->variationValueArray;
                
                $orderDetailsResponse = array();
                foreach($productArray as $key=> $product) {
                    $orderDetails = new OrderDetails;
                    $orderDetails->order_id = $order->id;
                    
                    $orderDetails->product_id = $productArray[$key];
                    
                    $orderDetails->quantity = $quantityArray[$key];
                    $orderDetails->variation_value_id = $variationValueArray[$key];
                    $orderDetails->save();

                    array_push($orderDetailsResponse, $orderDetails);
                }
                
            }

            $paymentIntent = $this->setData('data', $paymentIntent->toArray());
            $data = ['order' => $order, 'order_details' => $orderDetailsResponse, 'payment_details' => $paymentIntent['data']];  

            return response()->json(['status' => true, 'message' => 'Payment Successful and order placed!', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return response()->json(['orders' => Order::all()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    public function userOrders(Request $request) {
        try {
            $orders = DB::table('orders')
                        ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                        ->join('products', 'order_details.product_id', '=', 'products.id')
                        ->join('variation_values', 'order_details.variation_value_id', '=', 'variation_values.id')
                        ->select('orders.*', 'order_details.*', 'products.*', 'variation_values.*')
                        ->where('user_id', $request->id)
                        ->get();

            $orders = $this->setData('data', $orders->toArray());
            $data = ['orders' => $orders['data']];  

            return response()->json(['status' => true, 'message' => 'User orders', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function pendingShipments(Request $request) {
        try {
            $pendingShipments = Order::where('user_id', Auth::id())->where('shipping_status', 0)->get();

            $pendingShipments = $this->setData('data', $pendingShipments->toArray());
            $data = ['pending_shipments' => $pendingShipments['data']];  

            return response()->json(['status' => true, 'message' => 'Pending Shipments', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function finishedOrders() {
        try {
            $finishedOrders = Order::where('user_id', Auth::id())->where('order_status', 1)->get();

            $finishedOrders = $this->setData('data', $finishedOrders->toArray());
            $data = ['finished_orders' => $finishedOrders['data']];  

            return response()->json(['status' => true, 'message' => 'Finished Orders', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }
}
