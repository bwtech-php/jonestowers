<?php

namespace App\Http\Controllers;

use App\BusinessImage;
use Illuminate\Http\Request;

class BusinessImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BusinessImage  $businessImage
     * @return \Illuminate\Http\Response
     */
    public function show(BusinessImage $businessImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BusinessImage  $businessImage
     * @return \Illuminate\Http\Response
     */
    public function edit(BusinessImage $businessImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BusinessImage  $businessImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BusinessImage $businessImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BusinessImage  $businessImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(BusinessImage $businessImage)
    {
        //
    }
}
