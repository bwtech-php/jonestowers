<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Business;
use App\BusinessImage;
use App\Payment;
use App\Product;
use App\Order;
use App\Support;
use App\Suggestion;
use App\UserAddress;
use Auth;
use DB;
use Laravel\Cashier\Cashier;
use Illuminate\Support\Facades\URL;

//Mail
use Illuminate\Support\Facades\Mail;
use App\Mail\BusinessApproved;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function admin()
    {
        return view('admin');
    }

    public function users() {
        // return User::all();

        try {
            $users = User::with('addresses')->where('approved', 1)->where('role', '!=', 'business')->get();
            return response()->json(['status' => true, 'message' => 'All the Users', 'data' => $users]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function deleteUsers($id) {
        // $user = Product::find($id);
        // $user->delete();

        return User::find($id)->delete();

        return response()->json();
    }

    public function verifyPinCode(Request $request) {
        
        try {
            if($request->pin == Auth::user()->verification_pin) {
            $user = User::findorfail(Auth::id());
            $user->verified = 1;
            $user->save();
            $user = $this->setData('data', $user->toArray());
            $data = ['user' => $user['data']];
            return response()->json(['status' => true, 'message' => 'Pin successfully verified!', 'data' => $data]);
        }
        else {
            return response()->json(['status' => false, 'message' => 'Invalid pin! Please try again']);
        }
        } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
        
    }

    public function stripe(Request $request) {
        $stripeCustomer = Cashier::findBillable('cus_Ib5bMKKKSmaAWm');
        return $stripeCustomer->asStripeCustomer();
        $user = User::find(2);
        $stripeCustomer = $user->createAsStripeCustomer();
        return $stripeCustomer;
    }

    public function dash() {
        $totalEarnings = Payment::sum('amount');
        $orders = Order::count();
        $products = Product::count();
        $users = User::count();

        return response()->json(['totalEarnings' => $totalEarnings, 'orders' => $orders, 'products' => $products, 'users' => $users]);
    }

    public function supportInfo() {
        try {
            $support = Support::findorfail(1);

            $support = $this->setData('data', $support->toArray());
            $data = ['support' => $support['data']];
            return response()->json(['status' => true, 'message' => 'Support Information', 'data' => $data]);
        
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function suggestion(Request $request) {
        try {
            $suggestion = new Suggestion;
            $suggestion->user_id = Auth::id();
            $suggestion->subject = $request->subject;
            $suggestion->message = $request->message;
            $suggestion->save();

            $suggestion = $this->setData('data', $suggestion->toArray());
            $data = ['suggestion' => $suggestion['data']];
            return response()->json(['status' => true, 'message' => 'Suggestion saved successfully!', 'data' => $data]);
        
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function customerAddresses(Request $request) {
        try {
            $address = DB::table('user_addresses')
                        ->join('users', 'user_addresses.user_id', '=', 'users.id')
                        ->where('user_addresses.user_id', Auth::id())
                        ->select('user_addresses.*', 'users.name')
                        ->get();

            $address = $this->setData('data', $address->toArray());
            $data = ['address' => $address['data']];
            return response()->json(['status' => true, 'message' => 'User addresses', 'data' => $data]);
        
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function newAddress(Request $request) {
        try {
            $address = new UserAddress;
            $address->user_id = Auth::id();
            $address->address = $request->address;
            $address->save();

            $address = $this->setData('data', $address->toArray());
            $data = ['address' => $address['data']];
            return response()->json(['status' => true, 'message' => 'New User Address!', 'data' => $data]);
        
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function profile() {
        try {
            $profile = User::where('id', Auth::id())
                        ->with('business')->first();

            $profile = $this->setData('data', $profile->toArray());
            $data = ['user_profile' => $profile['data']];
            return response()->json(['status' => true, 'message' => 'User Profile!', 'data' => $data]);
        
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function profileEdit(Request $request) {
        
        
        try {
            
            // dd($request->file());
        
            // $test = new BusinessImage;
            
            // $image = $request->file('image');
            // $businessImage = "images/businesses/".$image->getClientOriginalName();
            // $path = $image->move(public_path('images/businesses'), $image->getClientOriginalName());
            // $test->business_id = 1;
            // $test->image = $businessImage;
            // $test->save();
            
            // return response()->json(['image' => $image]);
            
            if(Auth::user()->role == 'user') {
                $user = User::findorfail(Auth::id());
                $user->phone = $request->phone;
                $user->address = $request->address;
                $user->city = $request->city;
                $user->state = $request->state;
                $user->zip = $request->zip;
                $user->save();

                $user = $this->setData('data', $user->toArray());
                $data = ['user_profile' => $user['data']];
            }
            else if(Auth::user()->role == 'business') {
                
                $business = Business::where('user_id', Auth::id())->first();
                $deleteBusiness = BusinessImage::where('business_id', $business->id)->get();
                foreach($deleteBusiness as $businessImage) {
                    $businessImage->delete();
                }

                global $businessImage;
                if($request->hasFile('image')) {
                    foreach($request->file('image') as $image) {
                    
                        if ($request->hasFile('image')) {
                            $businessImage = "images/businesses/".$image->getClientOriginalName();
                            $path = $image->move(public_path('images/businesses'), $image->getClientOriginalName());
                        }
                        
                        $images = new BusinessImage;
                        $images->business_id = $business->id;
                        $images->image = $businessImage;
                        $images->save();
                    }
                }
                
                $business->name = $request->name;
                $business->number = $request->number;
                $business->url = $request->url;
                $business->description = $request->description;
                $business->address = $request->address;
                $business->city = $request->city;
                $business->state = $request->state;
                $business->zip = $request->zip;
                $business->category_id = $request->category_id;
                $business->save();

                $user = User::where('id', Auth::id())->with('business')->first();

                $user = $this->setData('data', $user->toArray());
                $data = ['user_profile' => $user['data']];

                if($request->hasFile('image')) {
                    $images = $this->setData('data', $images->toArray());
                    $data = ['user_profile' => $user['data'], 'images' => $images['data']];
                }
            }
            
            return response()->json(['status' => true, 'message' => 'Profile Edit!', 'data' => $data]);
        
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function approveUser(Request $request) {
        try {
            $user = User::findorfail($request->id);

            $business = Business::create([
                'user_id' => $user->id,
                'category_id' => $user->category_id,
                'name' => $user->user_name,
                'address' => $user->address,
                'city' => $user->city,
                'state' => $user->state,
                'zip' => $user->zip,
                'number' => $user->phone,
                'url' => $user->url,
                'african_american_owned' => $user->african_american_owned,
                'approved' => 1,
            ]);

            $user->approved = 1;
            $user->save();

            Mail::to($user)->send(new BusinessApproved());

            $user = $this->setData('data', $user->toArray());
            $business = $this->setData('data', $business->toArray());
            $data = ['approved_user' => $user['data'], 'approved_business' => $business['data']];
            return response()->json(['status' => true, 'message' => 'Approve User!', 'data' => $data]);
        
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function pendingUsers() {
        try {
            $users = User::where('approved', 0)->where('role', 'business')->with('business')->get();

            $users = $this->setData('data', $users->toArray());
            $data = ['pending_users' => $users['data']];
            return response()->json(['status' => true, 'message' => 'Pending Users', 'data' => $data]);
        
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }
}
