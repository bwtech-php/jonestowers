<?php

namespace App\Http\Controllers;

use App\Business;
use App\User;
use Illuminate\Http\Request;
use Auth;
use DB;

class BusinessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $businesses = Business::join('users', 'businesses.user_id', '=', 'users.id')
                                    ->join('categories', 'businesses.category_id', '=', 'categories.id')
                                    ->select('businesses.*', 'users.user_name', 'categories.name as category')
                                    ->get();

            // $businesses = Business::with('user', 'category', 'businessImages')->get();
            
            $businesses = $this->setData('data', $businesses->toArray());
            $data = ['businesses' => $businesses['data']];

            return response()->json(['status' => true, 'message' => 'Businesses', 'data' => $data]);
        } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function show(Business $business)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function edit(Business $business)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Business $business)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function destroy(Business $business)
    {
        //
    }

    public function search(Request $request) {
        try {
            // $businesses = DB::table('businesses')
            $businesses = Business::
            // ->join('sub_categories', 'sub_categories.id', '=', 'products.sub_category_id')
            where('name', 'LIKE', '%'.$request->search.'%')
            ->orwhere('address', 'LIKE', '%'.$request->search.'%')
            ->orwhere('state', 'LIKE', '%'.$request->search.'%')
            ->orwhere('city', 'LIKE', '%'.$request->search.'%')
            ->orwhere('zip', 'LIKE', '%'.$request->search.'%')
            ->orwhere('number', 'LIKE', '%'.$request->search.'%')
            ->orwhere('url', 'LIKE', '%'.$request->search.'%')
            ->orwhere('description', 'LIKE', '%'.$request->search.'%')
            ->select('businesses.*')
            ->with('reviews.ReviewImages', 'businessImages')
            ->latest()
            ->get();

            // $categories = DB::table('categories')
            // ->join('sub_categories', 'sub_categories.category_id', '=', 'categories.id')
            // ->where('categories.name', 'LIKE', '%'.$request->search.'%')
            // ->orwhere('sub_categories.name', 'LIKE', '%'.$request->search.'%')
            // ->select('categories.*', 'sub_categories.name as sub_category')
            // ->get();

            $businesses = $this->setData('data', $businesses->toArray());
            // $categories = $this->setData('categorydata', $categories->toArray());
            $data = ['businesses' => $businesses['data']];

            return response()->json(['status' => true, 'message' => 'Businesses', 'data' => $data]);
        } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }
}
