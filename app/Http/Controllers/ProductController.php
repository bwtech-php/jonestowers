<?php

namespace App\Http\Controllers;

use App\Product;
use App\Variation_Products;
use App\Category;
use App\outlet;
use App\Variation;
use App\Variation_value;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $products = Product::join('categories', 'products.category_id', 'categories.id')
        //                     ->select('products.*', 'categories.name as category_name')
        //                     ->with('variations')
        //                     ->get();
        
        // $variations = Variation::all();

        try {
            $products = Product::join('sub_categories', 'products.sub_category_id', 'sub_categories.id')
                        ->select('products.*', 'sub_categories.name as sub_category_name')
                        ->with('reviews', 'variations.variationvalues')
                        ->get();
            
            $products = $this->setData('data', $products->toArray());
            $data = ['products' => $products['data']];


            return response()->json(['status' => true, 'message' => 'All the Products', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
        
        // return response()->json((['products' => $products, 'variations' => $variations]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        
        global $productImage;
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $productImage = "images/products/".$request->file('image')->getClientOriginalName();
                $path = $request->image->move(public_path('images/products'), $request->file('image')->getClientOriginalName());
            }
        }
        
        $product = new Product;
        $product->name = $request->name;
        $product->description = $request->desc;
        $product->price = $request->price; 
        $product->category_id = $request->category;
        $product->image = $productImage;
        $product->save();

        // $productVariation = new Variation_Products;
        // $productVariation->product_id = $product->id;
        // $productVariation->variation_id = $product->variation_id;
        // $productVariation->save();

        return response()->json($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        try {
            $products = Product::join('sub_categories', 'products.sub_category_id', 'sub_categories.id')
                                ->where('products.id', $request->id)
                                ->select('products.*', 'sub_categories.name as sub_category_name')
                                ->with('productImages', 'reviews.reviewImages', 'variations.variationvalues', 'productDetails')
                                ->firstorfail();
            
            $products = $this->setData('data', $products->toArray());
            $data = ['prod_details' => $products['data']];


            return response()->json(['status' => true, 'message' => 'Product Details', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        return response()->json();
    }

    public function newProductInfo() {
        $categories = Category::all();
        $variations = Variation::with('variationValues')->get();

        return response()->json(['categories' => $categories, 'variations' => $variations]);
    }

    public function productVariations() {
        $product = Product::find(2)->with('variations')->get();

        return $product;
    }

    public function productInfo() {
        $products = Product::all();
        $categories = Category::all();
        $variations = Variation::with('variationValues')->get();
        $outlets = outlet::all();

        return response()->json([
            'products' => $products,
            'categories' => $categories,
            'variations' => $variations,
            'outlets' => $outlets
        ]);
    }

    public function search(Request $request) {
        
        
        try {
            $products = DB::table('products')
            ->join('sub_categories', 'sub_categories.id', '=', 'products.sub_category_id')
            ->where('products.name', 'LIKE', '%'.$request->search.'%')
            ->orwhere('products.description', 'LIKE', '%'.$request->search.'%')
            ->orwhere('products.price', 'LIKE', '%'.$request->search.'%')
            ->select('products.*', 'sub_categories.name as sub_category')
            ->get();

            $categories = DB::table('categories')
            ->join('sub_categories', 'sub_categories.category_id', '=', 'categories.id')
            ->where('categories.name', 'LIKE', '%'.$request->search.'%')
            ->orwhere('sub_categories.name', 'LIKE', '%'.$request->search.'%')
            ->select('categories.*', 'sub_categories.name as sub_category')
            ->get();

            $products = $this->setData('productdata', $products->toArray());
            $categories = $this->setData('categorydata', $categories->toArray());
            $data = ['products' => $products['productdata'], 'categories' => $categories['categorydata']];

            return response()->json(['status' => true, 'message' => 'Search Results', 'data' => $data]);
        } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        } 
    }

    public function ProductFilters(Request $request) {
        try {
            $products = Product::filter()
            ->with('productImages', 'reviews.reviewImages', 'variations.variationvalues', 'productDetails')
                        //         ->with(array('productImages'=>function($query){
                        //     $query->select('id','image as dfdfdf');
                        // }))
                        // ->select('products.*', 'productImages.image', 'reviews.reviewImages.image as review_images', 'variations.variationvalues.name as variation_values', 'productDetails.detail_value as product_details')
                        // ->select('products.*')
                        ->get();

            // return response()->json($products, 200);
            
            $products = $this->setData('data', $products->toArray());
            $data = ['prod_details' => $products['data']];


            return response()->json(['status' => true, 'message' => 'Product Details', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }
}
