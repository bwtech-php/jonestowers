<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variation extends Model
{
    public function variationValues()
    {
        return $this->hasMany('App\Variation_value', 'variation_id');
    }
}
