<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variation_value extends Model
{
    public function variations()
    {
        return $this->belongsTo('App\Variation');
    }
}
